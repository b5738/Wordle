make_reply_matrix <- function(dictionary=NULL, get_dictionary=T){
    # function to load dictionary (if needed) anc calcualte score matrix
    #note: quite slow

    require(doParallel)

    if(get_dictionary){
        dictionary <- read.csv(url("https://raw.githubusercontent.com/charlesreid1/five-letter-words/master/sgb-words.txt"), header=F)$V1
    }

    no_cores <- detectCores() - 1


    # Initiate cluster
    cl <- makeCluster(no_cores)
    doParallel::registerDoParallel(cl)

    # calculate score for all dictionary pairs
    dict_length<-length(dictionary)
    reply_matrix <- foreach(i = 1:dict_length, .combine = 'cbind', .packages="stringr", .export=c("score","score_matrix_calc")) %:%
        foreach(j = 1:dict_length, .combine = 'c', .packages="stringr") %dopar% {
            score_matrix_calc(i,j,dictionary)
        }

    # kill cluster
    stopCluster(cl = cl)

    # use symmetry to complete matrix
    for(i in 1:(dict_length-1)){
        for(j in (i+1):dict_length){
            reply_matrix[i,j] <-reply_matrix[j,i]
        }
    }

    # add names to matrix
    rownames(reply_matrix) <- colnames(reply_matrix) <- dictionary

    # calculate mean score for each word in dictionary
    reply_counts <- t(apply(reply_matrix,2,reply_frequency))
    colnames (reply_counts) <- rownames(reply_counts)
    diag(reply_counts) <- 0
    reply_means <- apply(reply_counts,1,mean)

    list(dictionary = dictionary, reply_matrix = reply_matrix, reply_means = reply_means)
}


score <- function(word1="frame",word2="great"){
    # function to calculate score return when hidden key is word1 and guess is word2

    require(stringr, quietly=T)

    word1_letters <- strsplit(word1, "")[[1]]
    word2_letters <- strsplit(word2, "")[[1]]
    matches <- word1_letters == word2_letters
    match_which <- which(matches)
    score1 <- sum(matches)

    word_return <- str_pad("", str_length(word1))

    if(score1>0)
        for(i in 1:score1)str_sub(word_return, match_which[i], match_which[i]) <-
        str_sub(word1, match_which[i], match_which[i])


    if(score1>0){
        word1_letters <- word1_letters[-match_which]
        word2_letters <- word2_letters[-match_which]
    }

    table_word1 <- table(word1_letters)
    table_word2 <- table(word2_letters)

    names_table1 <- names(table_word1)
    names_table2 <- names(table_word2)
    letters_common <- intersect(names_table1, names_table2)

    table_word1_common <- table_word1[letters_common]
    table_word2_common <- table_word2[letters_common]

    common_letter_count <- pmin(table_word1[letters_common], table_word2[letters_common])
    score2 <- sum(common_letter_count)

    if(score2 == 0){
        common_letters <- ""
    }
    else{
        common_letters <- rep(names(common_letter_count), common_letter_count)
    }

    word_return <- paste(word_return, paste(common_letters, collapse=''), sep=";")
    return(word_return)
}



score_matrix_calc <- function(i, j, dict){
    if(i > j){
        return(0)
    }
    else
    {
        return(score(dict[i], dict[j]))
    }
}

vscore_matrix_calc <- Vectorize(score_matrix_calc)

reply_frequency<-function(x){
    table_frequency <- table(x)
    table_frequency[match(x, names(table_frequency))]
}

wordle <- function(try, reply, reply_matrix_current, show_best=T, n_show_best=10){

    #make sure second part of reply is in alphabetical order

    w <- strsplit(reply,';')
    w2 <- strsplit(w[[1]][2],'')[[1]]%>%sort()%>%paste(collapse="")
    reply <- paste0(w[[1]][1],";",w2)

    #now identify remaining potential soultions
    matches <- reply_matrix_current[try,] == reply
    reply_matrix_new <- reply_matrix_current[matches,matches]

    if(class(reply_matrix_new)[1]=="character"){
        print(paste0("the solution is ", strsplit(reply_matrix_new,";")[[1]]))
        return(solution = reply_matrix_new)
    }

    if(is.null(dim(reply_matrix_new)))return(try)

    reply_counts <- t(apply(reply_matrix_new, 2, reply_frequency))
    diag(reply_counts) <- 0
    reply_means <- apply(reply_counts, 1, mean)

    if(show_best){
        n_show_best <- min(n_show_best, length(reply_means))
        cat(paste0('The best ',n_show_best, ' choices are:'), fill=T)
        cat(fill=T)
        print(sort(reply_means)[1:n_show_best] %>% round(2))
        cat(fill=T)
    }
    cat('The overall best choice(s) is/are:', fill=T)
    best_choices <- which(reply_means == min(reply_means))
    cat(names(best_choices), fill=T)

    list(best_choices = best_choices, reply_means = reply_means, reply_matrix = reply_matrix_new)
}


play_wordle <- function(try, key){
    attempts <- try
    round <- 1
    print(paste0("Round ",round))
    print(try)
    print(paste0("Score is ",score(try, key)))
    tmp <- wordle(try, score(try, key), dictionary_list$reply_matrix)
    while(length(tmp)>1){
        round <- round + 1
        cat(fill=T)
        print(paste0("Round ",round))
        word_try <- sample(names(tmp[[1]]), size=1)
        attempts <- c(attempts,word_try)
        print(word_try)
        print(paste0("Score is ",score(word_try, key)))
        tmp <- wordle(word_try,score(word_try,key),tmp[[3]])
        if(length(tmp)==1 && strsplit(tmp[1],";")[[1]]!=word_try){
            attempts <- c(attempts,strsplit(tmp[1],";")[[1]])
            round <- round+1
            break
        }
    }
    cat(fill=T)
    print(paste0("Solution found with ",round  , " attempts:"))
    print(attempts)

    return()
}

best_choices <- function(dictionary_list, n_show = 20){
    require(dplyr)
    sort(dictionary_list$reply_means) %>% head(n = n_show) %>% round(2)
}


